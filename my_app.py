from flask import Flask,request
import json
import dboperation

app = Flask(__name__)

db= dboperation.DbOperations

@app.route('/')
def index():
	response={"error":False,"message":"Hello World"}
	return json.dumps(response)

@app.route('/new_user',methods=['POST'])
def newUser():
	# response={}
	try:
		_name = request.form['inputName']
		_email = request.form['inputEmail']
		_password = request.form['inputPassword']

		result=db.userExistOrNot(_email)
		if result:
			response={"error":True,"message":"User already present"}
		else:
			res=db.insertUser(_name, _email, _password)
			response={"error":False,"message":"1 record inserted, ID:"+str(res)}
	except Exception as e:
		print(e)

	return json.dumps(response)

if __name__ == "__main__":
    app.run(debug=True)
