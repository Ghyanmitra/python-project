from dbconfig import mysql


class DbOperations(object):
    def userExistOrNot(email):
        sql = "SELECT name,email FROM users WHERE email=%s"
        data = (email)
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        row = cursor.fetchone()
        cursor.close()
        conn.close()
        return row

    def insertUser(_name, _email, _password):
        sql = "INSERT INTO users (name, email, password) VALUES(%s, %s, %s)"
        data = (_name, _email, _password)
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        conn.commit()
        cursor.close()
        conn.close()
        return cursor.lastrowid
